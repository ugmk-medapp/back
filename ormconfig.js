const {
  MSSQL_USERNAME,
  MSSQL_PASSWORD,
  MSSQL_HOSTNAME,
  MSSQL_PORT,
  MSSQL_DB
} = process.env;

module.exports = {
    "name": "default",
    "type": "mssql",
    "host": MSSQL_HOSTNAME,
    "port": +MSSQL_PORT,
    "username": MSSQL_USERNAME,
    "password": MSSQL_PASSWORD,
    "database": MSSQL_DB,
    "options": {
      "encrypt": true,
      "enableArithAbort": true
    },
    "entities": ["dist/entities/*{.ts,.js}"]
  }