import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Users } from "./Users";

@Index("IX_AspNetUserClaims_UserId", ["userId"], {})
@Index("PK_AspNetUserClaims", ["id"], { unique: true })
@Entity("AspNetUserClaims", { schema: "dbo" })
export class AspNetUserClaims {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("uniqueidentifier", { name: "UserId" })
  userId: string;

  @Column("nvarchar", { name: "ClaimType", nullable: true })
  claimType: string | null;

  @Column("nvarchar", { name: "ClaimValue", nullable: true })
  claimValue: string | null;

  @ManyToOne(() => Users, (users) => users.aspNetUserClaims, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "UserId", referencedColumnName: "id" }])
  user: Users;
}
