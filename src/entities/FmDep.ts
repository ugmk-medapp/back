import { Column, Entity, Index, OneToMany } from "typeorm";
import { OrganizationFmdep } from "./OrganizationFmdep";

@Index("PK_FmDep", ["id"], { unique: true })
@Entity("FmDep", { schema: "dbo" })
export class FmDep {
  @Column("uniqueidentifier", { primary: true, name: "Id" })
  id: string;

  @Column("int", { name: "FM_DEP_ID" })
  fmDepId: number;

  @Column("nvarchar", { name: "CODE", nullable: true })
  code: string | null;

  @Column("nvarchar", { name: "LABEL", nullable: true })
  label: string | null;

  @Column("bit", { name: "EXTERNAL_ORG", nullable: true })
  externalOrg: boolean | null;

  @Column("int", { name: "FM_ORG_ID", nullable: true })
  fmOrgId: number | null;

  @Column("int", { name: "DM_DEP_TYPES_ID", nullable: true })
  dmDepTypesId: number | null;

  @Column("int", { name: "MAIN_ORG_ID" })
  mainOrgId: number;

  @Column("int", { name: "MEDECINS_ID", nullable: true })
  medecinsId: number | null;

  @Column("int", { name: "FM_DEP_PROF_ID", nullable: true })
  fmDepProfId: number | null;

  @Column("int", { name: "DEP_TYPE", nullable: true })
  depType: number | null;

  @Column("bit", { name: "OBL_BAD", nullable: true })
  oblBad: boolean | null;

  @Column("bit", { name: "RAY_BAD", nullable: true })
  rayBad: boolean | null;

  @Column("bit", { name: "REANIM", nullable: true })
  reanim: boolean | null;

  @Column("bit", { name: "ARCHIVE", nullable: true })
  archive: boolean | null;

  @Column("datetime2", { name: "KRN_CREATE_DATE", nullable: true })
  krnCreateDate: Date | null;

  @Column("int", { name: "KRN_CREATE_USER_ID", nullable: true })
  krnCreateUserId: number | null;

  @Column("datetime2", { name: "KRN_MODIFY_DATE", nullable: true })
  krnModifyDate: Date | null;

  @Column("int", { name: "KRN_MODIFY_USER_ID", nullable: true })
  krnModifyUserId: number | null;

  @Column("int", { name: "KRN_CREATE_DATABASE_ID", nullable: true })
  krnCreateDatabaseId: number | null;

  @Column("int", { name: "KRN_MODIFY_DATABASE_ID", nullable: true })
  krnModifyDatabaseId: number | null;

  @Column("nvarchar", { name: "KRN_GUID", nullable: true })
  krnGuid: string | null;

  @Column("int", { name: "USE_BEDS_ON_SETTLING", nullable: true })
  useBedsOnSettling: number | null;

  @Column("int", { name: "CHECK_BEDS_QUANTITY", nullable: true })
  checkBedsQuantity: number | null;

  @Column("bit", { name: "SETTLE_TO_OTHER_DEP", nullable: true })
  settleToOtherDep: boolean | null;

  @Column("datetime2", { name: "InsertDate", nullable: true })
  insertDate: Date | null;

  @Column("datetime2", { name: "UpdateDate", nullable: true })
  updateDate: Date | null;

  @OneToMany(
    () => OrganizationFmdep,
    (organizationFmdep) => organizationFmdep.fmDep
  )
  organizationFmdeps: OrganizationFmdep[];
}
