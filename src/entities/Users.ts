import {
  Column,
  Entity,
  Index,
  JoinTable,
  ManyToMany,
  OneToMany,
} from "typeorm";
import { AspNetUserClaims } from "./AspNetUserClaims";
import { AspNetUserLogins } from "./AspNetUserLogins";
import { Roles } from "./Roles";
import { AspNetUserTokens } from "./AspNetUserTokens";

@Index("EmailIndex", ["normalizedEmail"], {})
@Index("PK_Users", ["id"], { unique: true })
@Index("UserNameIndex", ["normalizedUserName"], { unique: true })
@Entity("Users", { schema: "dbo" })
export class Users {
  @Column("uniqueidentifier", { primary: true, name: "Id" })
  id: string;

  @Column("nvarchar", { name: "Surname", nullable: true, length: 500 })
  surname: string | null;

  @Column("nvarchar", { name: "Name", nullable: true, length: 500 })
  name: string | null;

  @Column("nvarchar", { name: "Patronymic", nullable: true, length: 500 })
  patronymic: string | null;

  @Column("nvarchar", { name: "Position", nullable: true, length: 500 })
  position: string | null;

  @Column("nvarchar", { name: "UserName", nullable: true, length: 256 })
  userName: string | null;

  @Column("nvarchar", {
    name: "NormalizedUserName",
    nullable: true,
    length: 256,
  })
  normalizedUserName: string | null;

  @Column("nvarchar", { name: "Email", nullable: true, length: 256 })
  email: string | null;

  @Column("nvarchar", { name: "NormalizedEmail", nullable: true, length: 256 })
  normalizedEmail: string | null;

  @Column("bit", { name: "EmailConfirmed" })
  emailConfirmed: boolean;

  @Column("nvarchar", { name: "PasswordHash", nullable: true })
  passwordHash: string | null;

  @Column("nvarchar", { name: "SecurityStamp", nullable: true })
  securityStamp: string | null;

  @Column("nvarchar", { name: "ConcurrencyStamp", nullable: true })
  concurrencyStamp: string | null;

  @Column("nvarchar", { name: "PhoneNumber", nullable: true })
  phoneNumber: string | null;

  @Column("bit", { name: "PhoneNumberConfirmed" })
  phoneNumberConfirmed: boolean;

  @Column("bit", { name: "TwoFactorEnabled" })
  twoFactorEnabled: boolean;

  @Column("datetimeoffset", { name: "LockoutEnd", nullable: true })
  lockoutEnd: Date | null;

  @Column("bit", { name: "LockoutEnabled" })
  lockoutEnabled: boolean;

  @Column("int", { name: "AccessFailedCount" })
  accessFailedCount: number;

  @OneToMany(
    () => AspNetUserClaims,
    (aspNetUserClaims) => aspNetUserClaims.user
  )
  aspNetUserClaims: AspNetUserClaims[];

  @OneToMany(
    () => AspNetUserLogins,
    (aspNetUserLogins) => aspNetUserLogins.user
  )
  aspNetUserLogins: AspNetUserLogins[];

  @ManyToMany(() => Roles, (roles) => roles.users)
  @JoinTable({
    name: "AspNetUserRoles",
    joinColumns: [{ name: "UserId", referencedColumnName: "id" }],
    inverseJoinColumns: [{ name: "RoleId", referencedColumnName: "id" }],
    schema: "dbo",
  })
  roles: Roles[];

  @OneToMany(
    () => AspNetUserTokens,
    (aspNetUserTokens) => aspNetUserTokens.user
  )
  aspNetUserTokens: AspNetUserTokens[];
}
