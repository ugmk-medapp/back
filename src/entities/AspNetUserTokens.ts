import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { Users } from "./Users";

@Index("PK_AspNetUserTokens", ["userId", "loginProvider", "name"], {
  unique: true,
})
@Entity("AspNetUserTokens", { schema: "dbo" })
export class AspNetUserTokens {
  @Column("uniqueidentifier", { primary: true, name: "UserId" })
  userId: string;

  @Column("nvarchar", { primary: true, name: "LoginProvider", length: 450 })
  loginProvider: string;

  @Column("nvarchar", { primary: true, name: "Name", length: 450 })
  name: string;

  @Column("nvarchar", { name: "Value", nullable: true })
  value: string | null;

  @ManyToOne(() => Users, (users) => users.aspNetUserTokens, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "UserId", referencedColumnName: "id" }])
  user: Users;
}
