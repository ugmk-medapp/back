import { Column, Entity, Index } from "typeorm";

@Index("PK_SyncErrors", ["id"], { unique: true })
@Entity("SyncErrors", { schema: "dbo" })
export class SyncErrors {
  @Column("uniqueidentifier", { primary: true, name: "Id" })
  id: string;

  @Column("nvarchar", { name: "MethodName", nullable: true })
  methodName: string | null;

  @Column("datetime2", { name: "ErrorDate" })
  errorDate: Date;

  @Column("nvarchar", { name: "ErrorMessage" })
  errorMessage: string;
}
