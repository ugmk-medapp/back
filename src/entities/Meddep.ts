import { Column, Entity, Index, OneToMany } from "typeorm";
import { OrganizationMeddep } from "./OrganizationMeddep";

@Index("PK_Meddep", ["id"], { unique: true })
@Entity("Meddep", { schema: "dbo" })
export class Meddep {
  @Column("uniqueidentifier", { primary: true, name: "Id" })
  id: string;

  @Column("int", { name: "MEDDEP_ID" })
  meddepId: number;

  @Column("int", { name: "MEDECINS_ID" })
  medecinsId: number;

  @Column("int", { name: "FM_DEP_ID" })
  fmDepId: number;

  @Column("int", { name: "SPECIALISATION_ID", nullable: true })
  specialisationId: number | null;

  @Column("int", { name: "MEDECINS_GROUP_ID", nullable: true })
  medecinsGroupId: number | null;

  @Column("bit", { name: "ARCHIVE", nullable: true })
  archive: boolean | null;

  @Column("nvarchar", { name: "TABLE_NUMBER", nullable: true })
  tableNumber: string | null;

  @Column("decimal", { name: "RATE", nullable: true, precision: 18, scale: 2 })
  rate: number | null;

  @Column("int", { name: "MEDECINS_PROFILE_ID", nullable: true })
  medecinsProfileId: number | null;

  @Column("int", { name: "MODELS_ID", nullable: true })
  modelsId: number | null;

  @Column("bit", { name: "CHANGE_MOTCONSU_MED_DISABLED", nullable: true })
  changeMotconsuMedDisabled: boolean | null;

  @Column("datetime2", { name: "KRN_CREATE_DATE", nullable: true })
  krnCreateDate: Date | null;

  @Column("int", { name: "KRN_CREATE_USER_ID", nullable: true })
  krnCreateUserId: number | null;

  @Column("datetime2", { name: "KRN_MODIFY_DATE", nullable: true })
  krnModifyDate: Date | null;

  @Column("int", { name: "KRN_MODIFY_USER_ID", nullable: true })
  krnModifyUserId: number | null;

  @Column("int", { name: "KRN_CREATE_DATABASE_ID", nullable: true })
  krnCreateDatabaseId: number | null;

  @Column("int", { name: "KRN_MODIFY_DATABASE_ID", nullable: true })
  krnModifyDatabaseId: number | null;

  @Column("int", { name: "ED_MED_TYPE_ID", nullable: true })
  edMedTypeId: number | null;

  @Column("nvarchar", { name: "SMP_CODE", nullable: true })
  smpCode: string | null;

  @Column("int", { name: "FM_DEP_PROF_ID", nullable: true })
  fmDepProfId: number | null;

  @Column("datetime2", { name: "InsertDate", nullable: true })
  insertDate: Date | null;

  @Column("datetime2", { name: "UpdateDate", nullable: true })
  updateDate: Date | null;

  @OneToMany(
    () => OrganizationMeddep,
    (organizationMeddep) => organizationMeddep.meddep
  )
  organizationMeddeps: OrganizationMeddep[];
}
