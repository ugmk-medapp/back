import { Column, Entity, Index, OneToMany } from "typeorm";
import { OrganizationMedecins } from "./OrganizationMedecins";

@Index("PK_Medecins", ["id"], { unique: true })
@Entity("Medecins", { schema: "dbo" })
export class Medecins {
  @Column("uniqueidentifier", { primary: true, name: "Id" })
  id: string;

  @Column("int", { name: "MEDECINS_ID" })
  medecinsId: number;

  @Column("int", { name: "MEDECINS_COMPTABLE_ID", nullable: true })
  medecinsComptableId: number | null;

  @Column("int", { name: "N_AGENDA", nullable: true })
  nAgenda: number | null;

  @Column("int", { name: "N_MODELE", nullable: true })
  nModele: number | null;

  @Column("bit", { name: "N_USE_PLAN", nullable: true })
  nUsePlan: boolean | null;

  @Column("nvarchar", { name: "SV_LPS", nullable: true })
  svLps: string | null;

  @Column("int", { name: "N_DEFAULT_EXAM", nullable: true })
  nDefaultExam: number | null;

  @Column("nvarchar", { name: "AG2035_DIR", nullable: true })
  ag2035Dir: string | null;

  @Column("nvarchar", { name: "AG2035_COMPTE", nullable: true })
  ag2035Compte: string | null;

  @Column("bit", { name: "FORCER_MODELE", nullable: true })
  forcerModele: boolean | null;

  @Column("int", { name: "FM_DEP_ID", nullable: true })
  fmDepId: number | null;

  @Column("int", { name: "FM_ORG_ID", nullable: true })
  fmOrgId: number | null;

  @Column("int", { name: "FM_ROLE_ID", nullable: true })
  fmRoleId: number | null;

  @Column("nvarchar", { name: "SYSTEM_FLAGS", nullable: true })
  systemFlags: string | null;

  @Column("datetime2", { name: "DATE_EUROSTART", nullable: true })
  dateEurostart: Date | null;

  @Column("int", { name: "FM_PRICETYPE_ID", nullable: true })
  fmPricetypeId: number | null;

  @Column("int", { name: "RM_DATABASES_ID", nullable: true })
  rmDatabasesId: number | null;

  @Column("bit", { name: "OPEN_SERV_IN_BILL", nullable: true })
  openServInBill: boolean | null;

  @Column("int", { name: "MED_DOLZHNOST_ID", nullable: true })
  medDolzhnostId: number | null;

  @Column("bit", { name: "ALL_ORGS", nullable: true })
  allOrgs: boolean | null;

  @Column("bit", { name: "USE_ALL_WAREHOUSES", nullable: true })
  useAllWarehouses: boolean | null;

  @Column("int", { name: "SPECIALISATION_ID", nullable: true })
  specialisationId: number | null;

  @Column("int", { name: "MAIN_PASSWORD_AGING", nullable: true })
  mainPasswordAging: number | null;

  @Column("datetime2", { name: "MAIN_PASSWORD_DATE", nullable: true })
  mainPasswordDate: Date | null;

  @Column("nvarchar", { name: "ACTIVE_DIRECTORY_GUID", nullable: true })
  activeDirectoryGuid: string | null;

  @Column("bit", { name: "OVERRIDE_PROFILE_RIGHTS", nullable: true })
  overrideProfileRights: boolean | null;

  @Column("nvarchar", { name: "NOM", nullable: true })
  nom: string | null;

  @Column("nvarchar", { name: "PRENOM", nullable: true })
  prenom: string | null;

  @Column("nvarchar", { name: "TELEFON", nullable: true })
  telefon: string | null;

  @Column("nvarchar", { name: "SPECIALISATION", nullable: true })
  specialisation: string | null;

  @Column("nvarchar", { name: "KOD1", nullable: true })
  kod1: string | null;

  @Column("nvarchar", { name: "KOD2", nullable: true })
  kod2: string | null;

  @Column("nvarchar", { name: "KOD3", nullable: true })
  kod3: string | null;

  @Column("nvarchar", { name: "KOD4", nullable: true })
  kod4: string | null;

  @Column("nvarchar", { name: "KOD5", nullable: true })
  kod5: string | null;

  @Column("bit", { name: "ACCESSTOSV", nullable: true })
  accesstosv: boolean | null;

  @Column("nvarchar", { name: "KABINET", nullable: true })
  kabinet: string | null;

  @Column("bit", { name: "ARCHIVE", nullable: true })
  archive: boolean | null;

  @Column("bit", { name: "NOEMIE", nullable: true })
  noemie: boolean | null;

  @Column("bit", { name: "SECTEUR2", nullable: true })
  secteur2: boolean | null;

  @Column("bit", { name: "ActiverEntentePrealable", nullable: true })
  activerEntentePrealable: boolean | null;

  @Column("int", { name: "TYPE", nullable: true })
  type: number | null;

  @Column("nvarchar", { name: "ACCESS_CODE", nullable: true })
  accessCode: string | null;

  @Column("datetime2", { name: "KRN_CREATE_DATE", nullable: true })
  krnCreateDate: Date | null;

  @Column("int", { name: "KRN_CREATE_USER_ID", nullable: true })
  krnCreateUserId: number | null;

  @Column("datetime2", { name: "KRN_MODIFY_DATE", nullable: true })
  krnModifyDate: Date | null;

  @Column("int", { name: "KRN_MODIFY_USER_ID", nullable: true })
  krnModifyUserId: number | null;

  @Column("int", { name: "KRN_CREATE_DATABASE_ID", nullable: true })
  krnCreateDatabaseId: number | null;

  @Column("int", { name: "KRN_MODIFY_DATABASE_ID", nullable: true })
  krnModifyDatabaseId: number | null;

  @Column("nvarchar", { name: "KRN_GUID", nullable: true })
  krnGuid: string | null;

  @Column("bit", { name: "OVERRIDE_PROFILE_ATTACHMENTS", nullable: true })
  overrideProfileAttachments: boolean | null;

  @Column("nvarchar", { name: "EXT_N_OMON_FIELD", nullable: true })
  extNOmonField: string | null;

  @Column("bit", { name: "EXT_N_OMON_FIND", nullable: true })
  extNOmonFind: boolean | null;

  @Column("nvarchar", { name: "TABEL_N_J_NOMER", nullable: true })
  tabelNJNomer: string | null;

  @Column("datetime2", { name: "DATA_ROGDENIQ", nullable: true })
  dataRogdeniq: Date | null;

  @Column("int", { name: "POL", nullable: true })
  pol: number | null;

  @Column("nvarchar", { name: "DOKUMENT_UDOSTOVERQYHIJ_L", nullable: true })
  dokumentUdostoverqyhijL: string | null;

  @Column("nvarchar", { name: "ADRES", nullable: true })
  adres: string | null;

  @Column("nvarchar", { name: "ADRES_PROGIVANIQ", nullable: true })
  adresProgivaniq: string | null;

  @Column("nvarchar", { name: "MOBIL_N_J_TEL", nullable: true })
  mobilNJTel: string | null;

  @Column("nvarchar", { name: "INN", nullable: true })
  inn: string | null;

  @Column("nvarchar", { name: "POLIS_OMS", nullable: true })
  polisOms: string | null;

  @Column("datetime2", { name: "DATA_V_DAHI", nullable: true })
  dataVDahi: Date | null;

  @Column("datetime2", { name: "SROK_DEJSTVIQ", nullable: true })
  srokDejstviq: Date | null;

  @Column("int", { name: "REGION_STRA_OVANIQ", nullable: true })
  regionStraOvaniq: number | null;

  @Column("int", { name: "ORGANIZACIQ", nullable: true })
  organizaciq: number | null;

  @Column("nvarchar", { name: "NOMER_DOGOVORA", nullable: true })
  nomerDogovora: string | null;

  @Column("nvarchar", { name: "OBRAZOVANIE", nullable: true })
  obrazovanie: string | null;

  @Column("nvarchar", { name: "DOKUMENT_OB_OBRAZOVANII", nullable: true })
  dokumentObObrazovanii: string | null;

  @Column("nvarchar", { name: "SERTIFIKAT", nullable: true })
  sertifikat: string | null;

  @Column("nvarchar", { name: "KATEGORIQ", nullable: true })
  kategoriq: string | null;

  @Column("int", { name: "DOLGNOST", nullable: true })
  dolgnost: number | null;

  @Column("float", { name: "KOLIHESTVO_STAVOK", nullable: true, precision: 53 })
  kolihestvoStavok: number | null;

  @Column("datetime2", { name: "DATA_VSTUPLENIQ_V_DOLGNOS", nullable: true })
  dataVstupleniqVDolgnos: Date | null;

  @Column("datetime2", { name: "DATA_ZAVERHENIQ_RABOT", nullable: true })
  dataZaverheniqRabot: Date | null;

  @Column("float", { name: "PROCENT_V_RUHKI", nullable: true, precision: 53 })
  procentVRuhki: number | null;

  @Column("nvarchar", { name: "KOMMENTARIJ", nullable: true })
  kommentarij: string | null;

  @Column("nvarchar", { name: "INDEKS", nullable: true })
  indeks: string | null;

  @Column("nvarchar", { name: "GOROD", nullable: true })
  gorod: string | null;

  @Column("nvarchar", { name: "SNILS", nullable: true })
  snils: string | null;

  @Column("nvarchar", { name: "SYS_SNILS", nullable: true })
  sysSnils: string | null;

  @Column("nvarchar", { name: "EMAIL", nullable: true })
  email: string | null;

  @Column("nvarchar", { name: "CRT_ID", nullable: true })
  crtId: string | null;

  @Column("nvarchar", { name: "CRT_PROVIDER_CODE", nullable: true })
  crtProviderCode: string | null;

  @Column("int", { name: "CRT_ALG_ID", nullable: true })
  crtAlgId: number | null;

  @Column("nvarchar", { name: "CRT_STORAGE_CODE", nullable: true })
  crtStorageCode: string | null;

  @Column("bit", { name: "OVERRIDE_PROFILE_MODELS", nullable: true })
  overrideProfileModels: boolean | null;

  @Column("bit", { name: "OVERRIDE_PROFILE_DENIED_MODELS", nullable: true })
  overrideProfileDeniedModels: boolean | null;

  @Column("bit", { name: "OVERRIDE_PROFILE_PL_EXAM", nullable: true })
  overrideProfilePlExam: boolean | null;

  @Column("bit", { name: "OVERRIDE_PROFILE_RUBRICS", nullable: true })
  overrideProfileRubrics: boolean | null;

  @Column("bit", { name: "OVERRIDE_PROFILE_EXT_GUIDS", nullable: true })
  overrideProfileExtGuids: boolean | null;

  @Column("bit", { name: "OVERRIDE_PROFILE_WRHS", nullable: true })
  overrideProfileWrhs: boolean | null;

  @Column("bit", { name: "OVERRIDE_PROFILE_PAY_MODES", nullable: true })
  overrideProfilePayModes: boolean | null;

  @Column("bit", { name: "ALL_RUBR_ENABLE", nullable: true })
  allRubrEnable: boolean | null;

  @Column("bit", { name: "OVERRIDE_PROFILE_DENIED_CAT", nullable: true })
  overrideProfileDeniedCat: boolean | null;

  @Column("datetime2", { name: "InsertDate", nullable: true })
  insertDate: Date | null;

  @Column("datetime2", { name: "UpdateDate", nullable: true })
  updateDate: Date | null;

  @OneToMany(
    () => OrganizationMedecins,
    (organizationMedecins) => organizationMedecins.medecins
  )
  organizationMedecins: OrganizationMedecins[];
}
