import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { MedicalOrganizations } from "./MedicalOrganizations";
import { Meddep } from "./Meddep";

@Index("IX_OrganizationMeddep_MeddepId", ["meddepId"], {})
@Index(
  "IX_OrganizationMeddep_OrganizationId_MeddepId",
  ["organizationId", "meddepId"],
  { unique: true }
)
@Index("PK_OrganizationMeddep", ["id"], { unique: true })
@Entity("OrganizationMeddep", { schema: "dbo" })
export class OrganizationMeddep {
  @Column("uniqueidentifier", { primary: true, name: "Id" })
  id: string;

  @Column("uniqueidentifier", { name: "OrganizationId" })
  organizationId: string;

  @Column("uniqueidentifier", { name: "MeddepId" })
  meddepId: string;

  @ManyToOne(
    () => MedicalOrganizations,
    (medicalOrganizations) => medicalOrganizations.organizationMeddeps,
    { onDelete: "CASCADE" }
  )
  @JoinColumn([{ name: "OrganizationId", referencedColumnName: "id" }])
  organization: MedicalOrganizations;

  @ManyToOne(() => Meddep, (meddep) => meddep.organizationMeddeps, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "MeddepId", referencedColumnName: "id" }])
  meddep: Meddep;
}
