import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { Medecins } from "./Medecins";
import { MedicalOrganizations } from "./MedicalOrganizations";

@Index("IX_OrganizationMedecins_MedecinsId", ["medecinsId"], {})
@Index(
  "IX_OrganizationMedecins_OrganizationId_MedecinsId",
  ["organizationId", "medecinsId"],
  { unique: true }
)
@Index("PK_OrganizationMedecins", ["id"], { unique: true })
@Entity("OrganizationMedecins", { schema: "dbo" })
export class OrganizationMedecins {
  @Column("uniqueidentifier", { primary: true, name: "Id" })
  id: string;

  @Column("uniqueidentifier", { name: "OrganizationId" })
  organizationId: string;

  @Column("uniqueidentifier", { name: "MedecinsId" })
  medecinsId: string;

  @ManyToOne(() => Medecins, (medecins) => medecins.organizationMedecins, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "MedecinsId", referencedColumnName: "id" }])
  medecins: Medecins;

  @ManyToOne(
    () => MedicalOrganizations,
    (medicalOrganizations) => medicalOrganizations.organizationMedecins,
    { onDelete: "CASCADE" }
  )
  @JoinColumn([{ name: "OrganizationId", referencedColumnName: "id" }])
  organization: MedicalOrganizations;
}
