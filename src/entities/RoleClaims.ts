import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Roles } from "./Roles";

@Index("IX_RoleClaims_RoleId", ["roleId"], {})
@Index("PK_RoleClaims", ["id"], { unique: true })
@Entity("RoleClaims", { schema: "dbo" })
export class RoleClaims {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("uniqueidentifier", { name: "RoleId" })
  roleId: string;

  @Column("nvarchar", { name: "ClaimType", nullable: true })
  claimType: string | null;

  @Column("nvarchar", { name: "ClaimValue", nullable: true })
  claimValue: string | null;

  @ManyToOne(() => Roles, (roles) => roles.roleClaims, { onDelete: "CASCADE" })
  @JoinColumn([{ name: "RoleId", referencedColumnName: "id" }])
  role: Roles;
}
