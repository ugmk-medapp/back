import { Column, Entity, Index, OneToMany } from "typeorm";
import { OrganizationFmdep } from "./OrganizationFmdep";
import { OrganizationMeddep } from "./OrganizationMeddep";
import { OrganizationMedecins } from "./OrganizationMedecins";

@Index("PK_MedicalOrganizations", ["id"], { unique: true })
@Entity("MedicalOrganizations", { schema: "dbo" })
export class MedicalOrganizations {
  @Column("uniqueidentifier", { primary: true, name: "Id" })
  id: string;

  @Column("nvarchar", { name: "OrganizationName" })
  organizationName: string;

  @Column("nvarchar", { name: "ApiAddress" })
  apiAddress: string;

  @Column("nvarchar", { name: "UserName" })
  userName: string;

  @Column("nvarchar", { name: "Password" })
  password: string;

  @OneToMany(
    () => OrganizationFmdep,
    (organizationFmdep) => organizationFmdep.organization
  )
  organizationFmdeps: OrganizationFmdep[];

  @OneToMany(
    () => OrganizationMeddep,
    (organizationMeddep) => organizationMeddep.organization
  )
  organizationMeddeps: OrganizationMeddep[];

  @OneToMany(
    () => OrganizationMedecins,
    (organizationMedecins) => organizationMedecins.organization
  )
  organizationMedecins: OrganizationMedecins[];
}
