import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { Users } from "./Users";

@Index("IX_AspNetUserLogins_UserId", ["userId"], {})
@Index("PK_AspNetUserLogins", ["loginProvider", "providerKey"], {
  unique: true,
})
@Entity("AspNetUserLogins", { schema: "dbo" })
export class AspNetUserLogins {
  @Column("nvarchar", { primary: true, name: "LoginProvider", length: 450 })
  loginProvider: string;

  @Column("nvarchar", { primary: true, name: "ProviderKey", length: 450 })
  providerKey: string;

  @Column("nvarchar", { name: "ProviderDisplayName", nullable: true })
  providerDisplayName: string | null;

  @Column("uniqueidentifier", { name: "UserId" })
  userId: string;

  @ManyToOne(() => Users, (users) => users.aspNetUserLogins, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "UserId", referencedColumnName: "id" }])
  user: Users;
}
