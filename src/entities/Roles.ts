import { Column, Entity, Index, ManyToMany, OneToMany } from "typeorm";
import { Users } from "./Users";
import { RoleClaims } from "./RoleClaims";

@Index("PK_Roles", ["id"], { unique: true })
@Index("RoleNameIndex", ["normalizedName"], { unique: true })
@Entity("Roles", { schema: "dbo" })
export class Roles {
  @Column("uniqueidentifier", { primary: true, name: "Id" })
  id: string;

  @Column("nvarchar", { name: "Name", nullable: true, length: 256 })
  name: string | null;

  @Column("nvarchar", { name: "NormalizedName", nullable: true, length: 256 })
  normalizedName: string | null;

  @Column("nvarchar", { name: "ConcurrencyStamp", nullable: true })
  concurrencyStamp: string | null;

  @ManyToMany(() => Users, (users) => users.roles)
  users: Users[];

  @OneToMany(() => RoleClaims, (roleClaims) => roleClaims.role)
  roleClaims: RoleClaims[];
}
