import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { MedicalOrganizations } from "./MedicalOrganizations";
import { FmDep } from "./FmDep";

@Index("IX_OrganizationFmdep_FmDepId", ["fmDepId"], {})
@Index(
  "IX_OrganizationFmdep_OrganizationId_FmDepId",
  ["organizationId", "fmDepId"],
  { unique: true }
)
@Index("PK_OrganizationFmdep", ["id"], { unique: true })
@Entity("OrganizationFmdep", { schema: "dbo" })
export class OrganizationFmdep {
  @Column("uniqueidentifier", { primary: true, name: "Id" })
  id: string;

  @Column("uniqueidentifier", { name: "OrganizationId" })
  organizationId: string;

  @Column("uniqueidentifier", { name: "FmDepId" })
  fmDepId: string;

  @ManyToOne(
    () => MedicalOrganizations,
    (medicalOrganizations) => medicalOrganizations.organizationFmdeps,
    { onDelete: "CASCADE" }
  )
  @JoinColumn([{ name: "OrganizationId", referencedColumnName: "id" }])
  organization: MedicalOrganizations;

  @ManyToOne(() => FmDep, (fmDep) => fmDep.organizationFmdeps, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "FmDepId", referencedColumnName: "id" }])
  fmDep: FmDep;
}
