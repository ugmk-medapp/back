import { NestFactory } from '@nestjs/core';
import * as cookieParser from 'cookie-parser';
import { AppModule } from './app.module';

const frontUrl = process.env.FRONT_URL;

console.log('FRONT URL: ', frontUrl);

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({ 
    origin: [frontUrl, 'http://localhost:4000', 'http://192.168.0.17:4000'], 
    credentials: true
  });
  app.use(cookieParser());
  await app.listen(3000);
}
bootstrap();
