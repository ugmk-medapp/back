import { Body, Controller, Get, Param, Post, Query, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { AppointmentsService } from './appointments.service';
import { ConfirmDto } from './dto/confirm.dto';
import { GetAppointmentsDto } from './dto/get-appointments.dto';
import { GetTypesDto } from './dto/get-types.dto';

@Controller('appointments')
export class AppointmentsController {
    constructor(
        private readonly appointmentService: AppointmentsService,
    ) {}

    @UseGuards(JwtAuthGuard)
    @Get('types')
    async getAppTypes(@Query() query: GetTypesDto) {
        return this.appointmentService.getTypes(query.medDepId, query.externalMedDepId);
    };

    @UseGuards(JwtAuthGuard)
    @Get()
    async getApp(@Query() query: GetAppointmentsDto) {
        return this.appointmentService.getPatientsToService(query);
    }

    @UseGuards(JwtAuthGuard)
    @Post('confirm')
    async confirmAppointment(@Body() body: ConfirmDto) {
        await this.appointmentService.confirmAppointment(body);
    }

    @UseGuards(JwtAuthGuard)
    @Get('/service/:id')
    async getOneServiceApp(@Param('id') id: string, @Query('medDepId') medDepId: string) {
        return this.appointmentService.getOneServiceAppointment(medDepId, id);
    }

    @UseGuards(JwtAuthGuard)
    @Get('/schedule/:id')
    async getOneScheduleApp(@Param('id') id: string, @Query('medDepId') medDepId: string) {
        return this.appointmentService.getOneScheduleAppointment(medDepId, id);
    }
}
