import { HttpModule, Module } from '@nestjs/common';
import * as https from 'https';

import { AppointmentsService } from './appointments.service';
import { AppointmentsController } from './appointments.controller';
import { OrganizationsModule } from 'src/organizations/organizations.module';
import { FetchModule } from 'src/fetch/fetch.module';

@Module({
  imports:[
    HttpModule.register({
      httpsAgent: new https.Agent({ rejectUnauthorized: false })
    }),
    FetchModule.register({
      agent: new https.Agent({ rejectUnauthorized: false })
    }),
    OrganizationsModule
  ],
  providers: [AppointmentsService],
  controllers: [AppointmentsController]
})
export class AppointmentsModule {}
