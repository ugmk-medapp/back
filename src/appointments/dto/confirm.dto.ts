export class ConfirmDto {
    medDepId?: string;
    externalMedDepId?: number;
    appointmentId?: number;
    complaints?: string;
    program?: string;
    progression?: string; // прогрессия
    APbefore?: string; // AP - arterial pressure (Артериальное давление)
    APafter?: string;  // AP - arterial pressure (Артериальное давление)
    diagnosisId?: string;
    outcomeId?: string; // исход
    resultId?: string;
    diseaseHistory?: string; // история заболевания
    objectively?: string; // объективно
    recommendations?: string;
    isClose?: boolean;
    sendToLK?: boolean;
    expertAnamnesis?: string;
    diagnosisDescription?: string | null;
}