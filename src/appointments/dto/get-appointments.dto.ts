export class GetAppointmentsDto {
    type:  'service' | 'schedule';
    medDepId: string;
    externalMedDepId: number;
    page: number;
    searchName: string;
    appTypeId: number;
    dateFilter: string;
    serviceType: 0 | 1 | 2 // фильтр. все - 0, осмотр - 1, лфк - 2
}