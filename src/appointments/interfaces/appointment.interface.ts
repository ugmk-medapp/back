export interface Appointment {
    patient: { 
        patientId: number;
        name: string;
        surname: string;
        patronymic: string;
        birthYear: string;
        appointmentTime: string;
    },
    appTypeId: number;
    name: string;
    appointmentId: number; // я не знаю что это значит, но это нужно
    quantity: number;  //количество таких записей
    serviceType: 0 | 1 | 2;  // все - 0, осмотр - 1, лфк - 2
    comment: string;
}