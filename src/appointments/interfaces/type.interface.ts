export interface AppointmentType {
    appTypeId: number;
    name: string;
}