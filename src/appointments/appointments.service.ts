import { BadRequestException, HttpService, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common'; 

import { AppointmentType } from './interfaces/type.interface';
import { GetAppointmentsDto } from './dto/get-appointments.dto';
import { Appointment } from './interfaces/appointment.interface';
import { OrganizationsService } from 'src/organizations/organizations.service';
import { ConfirmDto } from './dto/confirm.dto';
import { FetchService } from 'src/fetch/fetch.service';


@Injectable()
export class AppointmentsService {
    constructor (
        private readonly organizationsService: OrganizationsService,
        private readonly httpService: HttpService,  // @TODO устарело, нужно заменить на fetchService
        private readonly fetchService: FetchService,
    ) {}

    async getTypes(medDepId: string, externalMedDepId: number): Promise<AppointmentType[]> {

        const { address, token } = await this.organizationsService.getOrganizationApi(medDepId);

        const response = await this.fetchService.get(
            `${address}GetModels/${externalMedDepId}`,
            {},
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": 'application/json'
                }
            }
        );

        const result: AppointmentType[] = (await response.json()).data.map(elem => {
            return {
                name: elem.modeleName,
                appTypeId:elem.models_ID
            }
        })

        return result;
    };

    async confirmAppointment(confirmDto: ConfirmDto) {

        console.log(confirmDto);

        const { address, token } = await this.organizationsService.getOrganizationApi(confirmDto.medDepId);

        const response = await this.fetchService.post(
            `${address}SetDirAnsw/${confirmDto.externalMedDepId}/${confirmDto.appointmentId}`,
            {
                adPosleLfk: confirmDto.APafter ? `${confirmDto.APafter}` : null,
                zhaloby: confirmDto.complaints || null,
                programmaLfk: confirmDto.program || null,
                progressiya: confirmDto.progression || null,
                adDoLfk: confirmDto.APbefore ? `${confirmDto.APbefore}` : null,
                outcomesId: +confirmDto.outcomeId || null,
                resultId: +confirmDto.resultId || null,
                motifDeConsultation: confirmDto.complaints || null,
                naznaheniq: confirmDto.recommendations || null,
                obhijOsmotr: confirmDto.objectively || null,
                dsId: confirmDto.diagnosisId ? `${confirmDto.diagnosisId}` : null,
                evClose: confirmDto.isClose,
                istoriqZabolevaniq: confirmDto.diseaseHistory || null,
                sendToLk: confirmDto.sendToLK,
                expertniyAnamnez: confirmDto.expertAnamnesis || null,
                dsDescription: confirmDto.diagnosisDescription || null,
            },
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            }
        );
        const result = await response.json();
        if(!response.ok) {
            console.log('[ERROR] confirm appointment response', response);
            if(response.status === 400 || response.status === 500) {
                throw new BadRequestException(result.data);
            }
            throw new InternalServerErrorException('error request confirm appointment api');
        } 
    }

    async getPatientsToService(appData: GetAppointmentsDto): Promise<Appointment[]> {
        const { medDepId, externalMedDepId, page, searchName, appTypeId, type, dateFilter, serviceType } = appData;

        const { address, token } = await this.organizationsService.getOrganizationApi(medDepId);

        const path = this.getPath(type);

        const data = {
            query: searchName || "",
            modelsId: appTypeId ? +appTypeId : null,
            date: dateFilter || null,
            type: +serviceType
        }

        const response = await this.fetchService.post(
            `${address}${path}${externalMedDepId}/${page}`,
            data,
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            }
        )

        if(!response.ok) {
            console.log(response);
            throw new InternalServerErrorException('get appointments api error');
        }
        const resAppointments = await response.json();
        const result = resAppointments.data.map(elem => {
            return this.toAppointment(elem, type);
        })

        return result;

    }

    async getOneServiceAppointment(medDepId: string, appointmentId: string):Promise<Appointment> {
        const { address, token } = await this.organizationsService.getOrganizationApi(medDepId);

        const response = await this.fetchService.post(
            `${address}GetDirAnsw/${appointmentId}`,
            {},
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            }
        )

        if(!response.ok) {
            console.log(response);
            throw new NotFoundException('appointment not found');
        }
        const resAppointment = await response.json();
        
        if(!resAppointment) {
            throw new NotFoundException('appointment not found');
        }
        const result =  this.toAppointment(resAppointment, 'service');

        return result;
    }

    async getOneScheduleAppointment(medDepId: string, appointmentId: string):Promise<Appointment> {
        const { address, token } = await this.organizationsService.getOrganizationApi(medDepId);

        const response = await this.fetchService.post(
            `${address}GetPlanDirAnsw/${appointmentId}`,
            {},
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            }
        )

        if(!response.ok) {
            console.log(response);
            throw new NotFoundException('appointment not found');
        }
        const resAppointment = await response.json();
        
        if(!resAppointment) {
            throw new NotFoundException('appointment not found');
        }
        const result =  this.toAppointment(resAppointment, 'service');

        return result;
    }

    private getPath(type: string):string {
        switch(type) {
            case 'service':
                return 'GetDirAnsw/';
            case 'schedule':
                return 'GetPlanDirAnsw/';
            default:
                return 'GetDirAnsw/';
        }
    }

    private toAppointment(rawApp, type:string): Appointment {
        const birthDate: string = rawApp.neLe;
        const birthYear = birthDate ? (birthDate.split('-'))[0] : 'неизвестен';

        let appointmentTime: string = 'неизвестно';

        if(rawApp.dateCons) {
           const appointmentTimeArr = rawApp.dateCons.split('T');
           const dateArr = appointmentTimeArr[0].split('-');
           const date = `${dateArr[2]}.${dateArr[1]}.${dateArr[0]}`;
           const timeArr = appointmentTimeArr[1].split(':');
           const time = `${timeArr[0]}:${timeArr[1]}`;

           appointmentTime = `${date} ${time}`;
        }

        if(type === 'service') {
            return {
                patient: { 
                    patientId: rawApp.patientsId, 
                    name: rawApp.prenom, 
                    surname: rawApp.nom, 
                    patronymic: rawApp.patronyme || '',  
                    birthYear: birthYear,
                    appointmentTime: null
                },
                appTypeId: rawApp.modelsId,
                name: rawApp.name,
                appointmentId: rawApp.dirAnswMinId,
                quantity: rawApp.cntNotDone,
                serviceType: rawApp.type || 1, // TODO не может быть null но на тесте может))))
                comment: rawApp.comment,
            }
        } else {
            return {
                patient: { 
                    patientId: rawApp.patientsId, 
                    name: rawApp.prenom, 
                    surname: rawApp.nom, 
                    patronymic: rawApp.patronyme || '',
                    birthYear: null,
                    appointmentTime: appointmentTime
                },
                appTypeId: rawApp.modelsId,
                name: rawApp.name,
                appointmentId: rawApp.dirAnswMinId,
                quantity: rawApp.cntNotDone || 1,
                serviceType: rawApp.type || 1, // TODO не может быть null но на тесте может))))
                comment: rawApp.comment,
            }
        }
    }
}
