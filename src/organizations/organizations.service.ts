import { BadRequestException, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { FetchService } from 'src/fetch/fetch.service';
import { EntityManager } from 'typeorm';
import { OrganizationsDB } from './db/organizations.db';
import { externalApiResponse } from './interfaces/external-api-response.interface';
import { OrganizationApi } from './interfaces/organization-api.interface';
import { Organization } from './interfaces/organization.interface';

@Injectable()
export class OrganizationsService {
    constructor(
        private readonly organizationsDB: OrganizationsDB,
        private readonly fetchService: FetchService,
        private readonly manager: EntityManager,
    ){}

    public async getOrganizationApi(organizationId): Promise<OrganizationApi> {
        const organization = await this.getOgranization(organizationId);
        const token = await this.getToken(organization);

        return {
            address: organization.apiAddress,
            token
        }
    }

    private async getToken(organization: Organization): Promise<string> {

        const response = await this.fetchService.post(
            `${organization.apiAddress}token`,
            {
                username: organization.username,
                password: organization.password
            },
            {
                headers: {
                    "Content-Type": 'application/json'
                }
            }
        )

        if(!response.ok) {
            console.log(response)
            throw new InternalServerErrorException('external api error');
        }

        const data: externalApiResponse = await response.json();

        return data.access_token;
    }

    private async getOgranization(organizationId: string): Promise<Organization> {

        const organization = await this.organizationsDB.findOne(organizationId, this.manager);

        if(!organization) {
            throw new BadRequestException('organization not found');
        }

        return organization;
    }
}
