import { BadRequestException, Injectable } from '@nestjs/common';
import { EntityManager } from 'typeorm';
import { Organization } from '../interfaces/organization.interface';

Injectable()
export class OrganizationsDB {

    async findOne(organizationId: string, manager: EntityManager): Promise<Organization | null> {
        const organizationRaw = (await manager.query(
            `SELECT 
                mo.Id,
                mo.ApiAddress,
                mo.UserName,
                mo.Password
            FROM OrganizationMeddep as om
                JOIN Meddep as md on md.id = om.MeddepId 
                JOIN MedicalOrganizations as mo on om.OrganizationId = mo.id
            WHERE md.id = @0`,
            [ organizationId ]
        ))[0]; // это точно вернет 1 элемент, так как выборка по уникальному id
        
        return this.toOrganizationType(organizationRaw);
    }

    private toOrganizationType(object): Organization | null {
        
        if(!object) {
            return null;
        }
        const { Id, ApiAddress, UserName, Password } = object;
        return {
            id: Id, 
            apiAddress: ApiAddress, 
            username: UserName, 
            password: Password
        };
    }
}