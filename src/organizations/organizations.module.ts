import { Module } from '@nestjs/common';
import { OrganizationsService } from './organizations.service';
import { OrganizationsDB } from './db/organizations.db';
import { FetchModule } from 'src/fetch/fetch.module';
import { Agent } from 'https';

@Module({
  imports: [FetchModule.register({
      agent: new Agent({ rejectUnauthorized: false }),
    })
  ],
  providers: [  
    OrganizationsDB,
    OrganizationsService, 
  ],
  exports:[OrganizationsService]
})
export class OrganizationsModule {}
