export interface Organization {
    id: string;
    apiAddress: string;
    username: string;
    password: string;
}