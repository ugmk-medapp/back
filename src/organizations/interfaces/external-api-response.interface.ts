export interface externalApiResponse {
    access_token: string,
    username: string,
    lifetime: string
}