import { Types } from "mongoose";

export type MongoId<Document> = string | Types.ObjectId | Document;