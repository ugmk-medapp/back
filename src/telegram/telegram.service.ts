import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class TelegramService {
    constructor(
        private readonly userService: UsersService,
        private jwtService: JwtService,
    ) {}

    async createLink(phone: string) {
        const user = await this.userService.createWithPhone(phone);

        const linkToken = this.jwtService.sign({
            email: user.email,
            phone: user.phone,
            sub: user._id
        }, { expiresIn: '2 h' });

        const frontUrl = process.env.FRONT_URL;

        return `${frontUrl}/login?link=${linkToken}`;
    }
}
