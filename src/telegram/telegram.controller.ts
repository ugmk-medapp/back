import { BadRequestException, Controller, ForbiddenException, Get, HttpException, HttpStatus, Post, Req, Res, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { UsersService } from 'src/users/users.service';
import { TelegramService } from './telegram.service';

@Controller('telegram')
export class TelegramController {
    constructor(
        private readonly telegramService: TelegramService,
        private readonly usersService: UsersService
    ){}

    @UseGuards(JwtAuthGuard)
    @Post('link')
    async createLink(@Req() req) {

        const user = await this.usersService.findById(req.user._id);

        if(!user?.is_telegram) {
            throw new ForbiddenException();
        }

        const phone = req.body.phone as string;

        if(!phone) throw new BadRequestException('Отсутствует телефон');

        const link = await this.telegramService.createLink(phone);

        return {
            link
        }
    }
}
