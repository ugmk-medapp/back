import { Module } from '@nestjs/common';
import { TelegramService } from './telegram.service';
import { TelegramController } from './telegram.controller';
import { UsersModule } from 'src/users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/auth/constants';

@Module({
  imports: [
    UsersModule,
    JwtModule.register({
      secret: jwtConstants.secretLink,
      signOptions: { expiresIn: '2 h' }
    })
  ],
  providers: [TelegramService],
  controllers: [TelegramController]
})
export class TelegramModule {}
