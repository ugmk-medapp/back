const JWT_KEY = process.env.JWT_KEY;
const JWT_KEY_LINK = process.env.JWT_KEY_LINK;

export const jwtConstants = {
    secret: JWT_KEY || 'secretKey',
    secretLink: JWT_KEY_LINK || 'super_secret_key',
}