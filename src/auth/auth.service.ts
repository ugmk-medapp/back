import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { UserDocument } from 'src/users/schemas/users.schema';
import { jwtConstants } from './constants';

@Injectable()
export class AuthService {
    constructor(
        private userServise: UsersService,
        private jwtService: JwtService,
    ) {}

    async validateUser(email: string, password: string): Promise<any> {
        const user = await this.userServise.findOneWithPassword(email);
        if(!user?.password) {
            return null;
        }

        if(user.is_verified) {
            const isMatch = await bcrypt.compare(password, user.password);
            if(isMatch) {
                return user;
            }
        }
        return null;
    }

    async validateUserWithLink(link: string): Promise<UserDocument | null> {
        
        try {
            const params = this.jwtService.verify(link, { secret: jwtConstants.secretLink });

            if(!params) return null;

            const user = await this.userServise.findById(params.sub);

            if(!user) return null;

            return user;
        } catch(err) {
            console.log(err);
            return null;
        }
    }

    login(user: any) {

        const payload = { email: user.email, sub: user._id, is_telegram: user.is_telegram };

        return {
            access_token: this.jwtService.sign(payload)
        }
    }
}
