import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { User, UserDocument } from "src/users/schemas/users.schema";
import { AuthService } from "./auth.service";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(
        private authService: AuthService
    ) {
        super({ usernameField: 'email' });
    }

    async validate(email: string, password: string): Promise<any> {

        let user: User | UserDocument;
        if(email === 'link') {
            user = await this.authService.validateUserWithLink(password);
        } else {
            user = await this.authService.validateUser(email, password);
        }
        if (!user) {
            throw new UnauthorizedException();
          }
          return user;
    }
}