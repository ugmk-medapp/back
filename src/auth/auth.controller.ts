import { Controller, Get, Post, Req, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Req() req, @Res({ passthrough: true }) res: Response) {
    const token = this.authService.login(req.user);
    const today = new Date();
    const inWeek = new Date();
    inWeek.setDate(today.getDate() + 7);
    res
      .cookie('access_token', token.access_token, { 
        httpOnly: true,
        expires: inWeek
      })
      .json(token);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/logout')
  async logout(@Res({ passthrough: true }) res: Response) {
    res.clearCookie('access_token');
  }


  @UseGuards(AuthGuard('jwt'))
  @Get('profile')
  getProfile(@Req() req) {
      return req.user;
  }
}
