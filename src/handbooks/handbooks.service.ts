import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { FetchService } from 'src/fetch/fetch.service';
import { OrganizationsService } from 'src/organizations/organizations.service';
import { DiagnosisApi } from './interfaces/diagnosis-api.interface';
import { Diagnosis } from './interfaces/Diagnosis.interface';

import { Outcome } from './interfaces/outcome.interface';
import { Result } from './interfaces/result.interface';

@Injectable()
export class HandbooksService {
    constructor(
        private readonly organizationsService: OrganizationsService,
        private readonly fetchService: FetchService
    ){}

    async getResults(medDepId: string): Promise<Result[]> {
        const { address, token } = await this.organizationsService.getOrganizationApi(medDepId);

        const response = await this.fetchService.get(
            `${address}GetOutcomes`,
            {},
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            }
        )

        if(!response.ok) {
            console.log(response);
            throw new InternalServerErrorException('external api get results error');
        }

        const resultsApi = await response.json();
        return this.toResult(resultsApi);
    }

    async getResultById(medDepId: string, resultId: number): Promise<Result> {
        if(!resultId) return null;

        const results = await this.getResults(medDepId);

        const result: Result = results.find(result => result.resultId === resultId);

        return result || null;
    }

    async getOutcomes(medDepId: string): Promise<Outcome[]> {
        const { address, token } = await this.organizationsService.getOrganizationApi(medDepId);

        const response = await this.fetchService.get(
            `${address}GetTreatment`,
            {},
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            }
        )

        if(!response.ok) {
            console.log(response);
            throw new InternalServerErrorException('external api get outcomes error');
        }

        const outcomesApi = await response.json();
        return this.toOutcome(outcomesApi);
    }

    async getOutcomeById(medDepId: string, outcomeId: number): Promise<Outcome> {
        if(!outcomeId) return null;

        const outcomes = await this.getOutcomes(medDepId);

        const outcome: Outcome = outcomes.find(outcome => outcome.outcomeId === outcomeId);

        return outcome || null;
    }

    async findDiagnoses(medDepId: string, searchString: string, page: number): Promise<Diagnosis[]> {
        const { address, token } = await this.organizationsService.getOrganizationApi(medDepId);

        const response = await this.fetchService.post(
            `${address}GetDiagnosisByPhrase?page=${page || 1}`,
            {
                query: searchString || null
            },
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            }
        )

        if(!response.ok) {
            console.log(response);
            throw new InternalServerErrorException('external api find diagnoses error');
        }

        const diagnosesApiData = await response.json();

        const diagnosesApi: DiagnosisApi[] = diagnosesApiData.data;

        const diagnoses = diagnosesApi.map(elem => this.toDiagnosis(elem));

        for(let diagnosis of diagnoses) {
            diagnosis.forbidden = await this.getForbidden(diagnosis.diagnosisId, address, token);
        }

        return diagnoses;
    }

    async getDiagnosesByParent(medDepId: string, parentId: string, page): Promise<Diagnosis[]> {
        const { address, token } = await this.organizationsService.getOrganizationApi(medDepId);

        let query: any = {
            page: page || 1,
        }

        if(parentId) query.parent = parentId;


        const response = await this.fetchService.get(
            `${address}GetDiagnosisByParent`,
            query,
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            }
        )

        if(!response.ok) {
            console.log(response);
            throw new InternalServerErrorException('external api get diagnoses by parent error');
        }

        const diagnosesApiData = await response.json();

        const diagnosesApi: DiagnosisApi[] = diagnosesApiData.data;

        const diagnoses = diagnosesApi.map(elem => this.toDiagnosis(elem));

        for(let diagnosis of diagnoses) {
            diagnosis.forbidden = await this.getForbidden(diagnosis.diagnosisId, address, token);
        }

        return diagnoses;
    }

    async getDiagnosisById(medDepId: string, id: number | string): Promise<Diagnosis> {

        if(!id) return null;

        const { address, token } = await this.organizationsService.getOrganizationApi(medDepId);

        const response = await this.fetchService.get(
            `${address}GetDiagnosis/${id}`,
            {},
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            }
        );
        if(!response.ok) {
            console.log(response);
            throw new InternalServerErrorException('external api get diagnoses by parent error');
        }

        if(response.status == 204) {
            return null;
        }

        const diagnosisApi: DiagnosisApi = await response.json();

        let diagnosis = this.toDiagnosis(diagnosisApi);
        
        diagnosis.forbidden = await this.getForbidden(diagnosis.diagnosisId, address, token);

        return diagnosis;
    }
    

    private async getForbidden(diagnosisId: number, address: string, token: string ): Promise<boolean> {
        const response = await this.fetchService.get(
            `${address}GetDiagnosisByParent`,
            {
                page: 1,
                parent: diagnosisId
            },
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            }
        )

        if(!response.ok) {
            console.log(response);
            throw new InternalServerErrorException('external api get diagnoses by parent error');
        }

        const diagnosesApi = await response.json();
        return diagnosesApi.data.length !== 0;
    }


    //TODO написать тип для апишных результатов. сейчас хз что выводить
    private toResult(resultsApi): Result[] {
        const { data } = resultsApi;
        const results = data.map(elem => ({
            resultId: elem.o_id,
            title: elem.name
        }));
        return results;
    }

    //TODO написать тип для апишных результатов. сейчас хз что выводить
    private toOutcome(outcomesApi): Outcome[] {
        const { data } = outcomesApi;
        const outcomes = data.map(elem => ({
            outcomeId: elem.t_id,
            title: elem.name
        }));
        return outcomes;
    }

    private toDiagnosis(diagnosisApi: DiagnosisApi): Diagnosis {

        return {
            diagnosisId: diagnosisApi.id,
            parentId: diagnosisApi.parent,
            code: diagnosisApi.code,
            title: diagnosisApi.description,
            forbidden: diagnosisApi.forbidden
        };
    }

}
