export interface Outcome {
    outcomeId: number;
    title: string;
}