export interface Diagnosis {
    diagnosisId: number;
    parentId: number;
    title: string;
    code: string;
    forbidden: boolean;
}