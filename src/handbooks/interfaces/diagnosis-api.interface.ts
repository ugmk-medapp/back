export interface DiagnosisApi {
    parent: number,
    id: number,
    timestamp?: string,
    code: string,
    version?: string,
    description: string,
    forbidden: boolean
}