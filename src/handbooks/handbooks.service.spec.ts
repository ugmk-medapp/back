import { Test, TestingModule } from '@nestjs/testing';
import { HandbooksService } from './handbooks.service';

describe('HandbooksService', () => {
  let service: HandbooksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HandbooksService],
    }).compile();

    service = module.get<HandbooksService>(HandbooksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
