import { Module } from '@nestjs/common';

import { FetchModule } from 'src/fetch/fetch.module';
import { Agent } from 'https';

import { OrganizationsModule } from 'src/organizations/organizations.module';
import { HandbooksService } from './handbooks.service';
import { HandbooksController } from './handbooks.controller';

@Module({
  imports: [
    FetchModule.register({
      agent: new Agent({rejectUnauthorized: false }),
    }),
    OrganizationsModule,
  ],
  providers: [HandbooksService],
  controllers: [HandbooksController],
  exports: [ HandbooksService ]
})
export class HandbooksModule {}
