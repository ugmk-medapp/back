import { Test, TestingModule } from '@nestjs/testing';
import { HandbooksController } from './handbooks.controller';

describe('HandbooksController', () => {
  let controller: HandbooksController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HandbooksController],
    }).compile();

    controller = module.get<HandbooksController>(HandbooksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
