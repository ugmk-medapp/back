export class DiagnosesByParentDto {
    parentId: string;
    medDepId: string;
    page: number;
}