export class FindDiagnosesDto {
    searchString: string;
    medDepId: string;
    page: number;
}