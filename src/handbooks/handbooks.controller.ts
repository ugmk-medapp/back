import { Query, Controller, Get, Param, UseGuards } from '@nestjs/common';

import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { HandbookDto } from './dto/handbook.dto';
import { HandbooksService } from './handbooks.service';
import { Result } from './interfaces/result.interface';
import { Outcome } from './interfaces/outcome.interface';
import { FindDiagnosesDto } from './dto/find-diagnosis.dto';
import { Diagnosis } from './interfaces/Diagnosis.interface';
import { DiagnosesByParentDto } from './dto/diagnoses-by-parent.dto';

@Controller('handbooks')
export class HandbooksController {
    constructor(
        private readonly handbooksService: HandbooksService,
    ){}

    @UseGuards(JwtAuthGuard)
    @Get('results')
    async getResultsHandbook(@Query() query: HandbookDto): Promise<Result[]> {
        const result = await this.handbooksService.getResults(query.medDepId);
        return result;
    }

    @UseGuards(JwtAuthGuard)
    @Get('outcomes')
    async getOutcomesHandbook(@Query() query: HandbookDto): Promise<Outcome[]> {
        const result = await this.handbooksService.getOutcomes(query.medDepId);
        return result;
    }

    @UseGuards(JwtAuthGuard)
    @Get('findDiagnoses')
    async findDiagnoses(@Query() query: FindDiagnosesDto): Promise<Diagnosis[]> {
        const result = await this.handbooksService.findDiagnoses(query.medDepId, query.searchString, query.page);
        return result;
    }

    @UseGuards(JwtAuthGuard)
    @Get('diagnosesByParent')
    async getDiagnosesByParent(@Query() query: DiagnosesByParentDto): Promise<Diagnosis[]> {
        const result = await this.handbooksService.getDiagnosesByParent(query.medDepId, query.parentId, query.page);
        return result;
    }
}
