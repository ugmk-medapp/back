import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { EmailService } from './email.service';
import { EmailController } from './email.controller';

@Module({
    imports: [
        MailerModule.forRoot({
            transport: {
                host: 'mail.phardoc.com',
                secure: false, // true for 465, false for other ports
                auth: {
                    user: 'mobidoc@phardoc.com',
                    pass: 'FTaLUhqxJaXh7T9'
                },
                tls: {
                    rejectUnauthorized: false
                }
            }
          }),
    ],
    providers: [EmailService],
    controllers: [EmailController],
    exports: [EmailService],
})
export class EmailModule {}
