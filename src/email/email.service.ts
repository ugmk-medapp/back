import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class EmailService {
    constructor(private readonly mailerService: MailerService) {}

    async sendEmail(to: string, subject: string, html: string): Promise<any> {
        return this.mailerService.sendMail({
            to: 'klabukov.514@gmail.com',
            from: 'mobidoc@phardoc.com',
            subject: subject, // Subject line
            html: html, // HTML body content
        })
    }

    public async sendEmailAfterCreateUser(to: string, code: string):Promise<any> {
        return this.mailerService.sendMail({
            to: to,
            from: 'mobidoc@phardoc.com',
            subject: 'Регистрация в личном кабинете MedApp (подтвердите email)', // Subject line
            html: `
                    <div style="padding: 20px;">
                        <h3>Добрый день!</h3>
                        <p style="font-size:16px;">Вы зарегестрировались в личном кабинете MedApp</p>
                        <p style="font-size:16px;">Для продолжения пользования сервисом MedApp необходимо подтвердить email. 
                        <p style="font-size:16px;">Для этого введите следующий код в приложении:</p>
                        <p style="font-size:30px; letter-spacing: 3px;"><strong>${code}</strong></p>
                    </div>
            `, // HTML body content
        })
    }

    public async sendEmailRestorePassword(to: string, code: string):Promise<any> {
        return this.mailerService.sendMail({
            to: to,
            from: 'mobidoc@phardoc.com',
            subject: 'Попытка восстановления пароля в личном кабинете MedApp', // Subject line
            html: `
                    <div style="padding: 20px;">
                        <h3>Добрый день!</h3>
                        <p style="font-size:16px;">Произошла попытка восстановления пароля в вашем личном кабинете MedApp</p>
                        <p style="font-size:16px;"> Чтобы мы были уверены, что это делате вы, мы просим вас подтвердить свой email. 
                        <p style="font-size:16px;">Для этого введите следующий код в приложении:</p>
                        <p style="font-size:30px; letter-spacing: 3px;"><strong>${code}</strong></p>
                    </div>
            `, // HTML body content
        })
    }


}
