import { Module } from '@nestjs/common';
import {ConfigModule} from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { EmailModule } from './email/email.module';
import { AppointmentsModule } from './appointments/appointments.module';
import { PatientsModule } from './patients/patients.module';
import { OrganizationsModule } from './organizations/organizations.module';
import { FetchModule } from './fetch/fetch.module';
import { HandbooksModule } from './handbooks/handbooks.module';
import { CardTemplateModule } from './card-template/card-template.module';
import { TelegramModule } from './telegram/telegram.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(`mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOSTNAME}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`),
    TypeOrmModule.forRoot(),
    AuthModule, 
    UsersModule, 
    EmailModule, 
    OrganizationsModule, 
    FetchModule,
    AppointmentsModule, 
    PatientsModule, HandbooksModule, CardTemplateModule, TelegramModule, 
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
