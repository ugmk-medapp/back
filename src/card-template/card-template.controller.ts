import { Body, Controller, Delete, Get, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CardTemplateService } from './card-template.service';
import { CardTemplateDto } from './dto/card-template.dto';
import { GetTemplatesDto } from './dto/get-templates.dto';

@Controller('cardTemplates')
export class CardTemplateController {
    constructor(
        private cardTemplateService: CardTemplateService,
    ) {}

    @UseGuards(JwtAuthGuard)
    @Post()
    async create(@Body() body: CardTemplateDto, @Req() req: any) {
        const userId: string = req.user._id;
        const result = await this.cardTemplateService.create(body, userId)

        return result;
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    async get(@Query() query: GetTemplatesDto, @Req() req: any) {
        const userId: string = req.user._id;
        const result = await this.cardTemplateService.get(query.serviceType, userId);

        return result;
    }

    @UseGuards(JwtAuthGuard)
    @Get('/:id')
    async getOne(@Param('id') id: string) {

        const result = await this.cardTemplateService.getOne(id);

        return result;
    }

    @UseGuards(JwtAuthGuard)
    @Delete('/:id')
    async delete(@Param('id') id: string) {
        const result = await this.cardTemplateService.delete(id);
        return result;
    }
}
