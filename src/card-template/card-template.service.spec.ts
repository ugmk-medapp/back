import { Test, TestingModule } from '@nestjs/testing';
import { CardTemplateService } from './card-template.service';

describe('CardTemplateService', () => {
  let service: CardTemplateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CardTemplateService],
    }).compile();

    service = module.get<CardTemplateService>(CardTemplateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
