import { ServiceType } from "../models/service-type.enum";

export class CardTemplateDto {
    medDepId: string;

    serviceType: ServiceType;
    title: string;
    complaints?: string;

    //ЛФК
    program?: string;
    progression?: string;
    APbefore?: string;
    APafter?: string;

    //Осмотр
    diagnosisId?: number;
    diagnosisDescription?: string;
    outcomeId?: number;
    resultId?: number;
    diseaseHistory?: string;
    objectively?: string;
    recommendations?: string;
    isClose?: boolean;
    expertAnamnesis?: string;

}
