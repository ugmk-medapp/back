import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HandbooksModule } from 'src/handbooks/handbooks.module';
import { CardTemplateController } from './card-template.controller';
import { CardTemplateService } from './card-template.service';
import { CardTemplate, CardTemplateSchema } from './schemas/card-template.schema';
import { CheckupTemplate, CheckupTemplateSchema } from './schemas/checkup-template.schema';
import { LfkTemplate, LfkTemplateSchema } from './schemas/lfk-template.schema';

@Module({
	imports: [
		MongooseModule.forFeature([
			{
				name: CardTemplate.name,
				schema: CardTemplateSchema,
				discriminators: [
					{ name: CheckupTemplate.name, schema: CheckupTemplateSchema },
					{ name: LfkTemplate.name, schema: LfkTemplateSchema }
				]
			}
		]),
		HandbooksModule
	],
	controllers: [CardTemplateController],
	providers: [CardTemplateService]
})
export class CardTemplateModule {}
