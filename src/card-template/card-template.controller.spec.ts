import { Test, TestingModule } from '@nestjs/testing';
import { CardTemplateController } from './card-template.controller';

describe('CardTemplateController', () => {
  let controller: CardTemplateController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CardTemplateController],
    }).compile();

    controller = module.get<CardTemplateController>(CardTemplateController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
