import { Prop, Schema } from "@nestjs/mongoose";


@Schema({_id: false})
export class Outcome {

    @Prop({
        type: Number
    })
    outcomeId?: number;

    @Prop({
        type: String,
    })
    title?: string;
}