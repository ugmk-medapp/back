import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { User, UserId } from "src/users/schemas/users.schema";
import { ServiceType } from '../models/service-type.enum';
import { MongoId } from 'src/models/mongo-id.interface';

export type CardTemplateDocument = CardTemplate & mongoose.Document;

export type CardTemplateId = MongoId<CardTemplateDocument>;

@Schema({ discriminatorKey: 'kind' })
export class CardTemplate {

    @Prop({
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    })
    user: UserId;

    @Prop({
        type: Number,
        required: true
    })
    serviceType: ServiceType;

    @Prop({
        type: String,
        required: true
    })
    title: string;

    @Prop({
        type: String
    })
    complaints?: string;

}

export const CardTemplateSchema = SchemaFactory.createForClass(CardTemplate);