import { Prop, Schema } from "@nestjs/mongoose";


@Schema({_id: false})
export class Diagnosis {

    @Prop({
        type: Number
    })
    diagnosisId?: number;

    @Prop({
        type: Number,
    })
    parentId?: number;

    @Prop({
        type: String,
    })
    code?: string;

    @Prop({
        type: String,
    })
    title?: string;

    @Prop({
        type: Boolean,
    })
    forbidden?: boolean;
}