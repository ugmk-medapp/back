import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import { MongoId } from "src/models/mongo-id.interface";
import { UserId } from "src/users/schemas/users.schema";
import { ServiceType } from "../models/service-type.enum";
import { Diagnosis } from "./diagnosis.nested.schema";
import { Outcome } from "./outcome.nested.schema";
import { Result } from "./result.nested.schema";

export type CheckupTemplateDocument = CheckupTemplate & Document;

export type CheckupTemplateId = MongoId<CheckupTemplateDocument>;

@Schema()
export class CheckupTemplate {

    user: UserId;
    serviceType: ServiceType;
    title: string;
    complaints?: string;

    @Prop({
        type: Diagnosis
    })
    diagnosis?: Diagnosis;

    @Prop({
        type: String
    })
    diagnosisDescription?: string;
    
    @Prop({
        type: Outcome
    })
    outcome?: Outcome;

    @Prop({
        type: Result
    })
    result?: Result;

    @Prop({
        type: String
    })
    diseaseHistory?: string;

    @Prop({
        type: String
    })
    objectively?: string;

    @Prop({
        type: String
    })
    recommendations?: string;

    @Prop({
        type: String
    })
    isClose?: boolean;

    @Prop({
        type: String
    })
    expertAnamnesis?: string;
}

export const CheckupTemplateSchema = SchemaFactory.createForClass(CheckupTemplate);