import { Prop, Schema } from "@nestjs/mongoose";


@Schema({_id: false})
export class Result {

    @Prop({
        type: Number
    })
    resultId?: number;

    @Prop({
        type: String,
    })
    title?: string;
}