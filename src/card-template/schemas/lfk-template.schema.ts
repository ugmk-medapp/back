import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import { MongoId } from "src/models/mongo-id.interface";
import { UserId } from "src/users/schemas/users.schema";
import { ServiceType } from "../models/service-type.enum";

export type LfkTemplateDocument = LfkTemplate & Document;

export type LfkTemplateId = MongoId<LfkTemplateDocument>;

@Schema()
export class LfkTemplate {

    user: UserId;
    serviceType: ServiceType;
    title: string;
    complaints?: string;

    @Prop({
        type: String
    })
    program?: string;

    @Prop({
        type: String
    })
    progression?: string;

    @Prop({
        type: String
    })
    APbefore?: string;

    @Prop({
        type: String
    })
    APafter?: string;
}

export const LfkTemplateSchema = SchemaFactory.createForClass(LfkTemplate);