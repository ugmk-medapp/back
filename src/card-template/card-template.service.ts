import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { HandbooksService } from 'src/handbooks/handbooks.service';
import { UserId } from 'src/users/schemas/users.schema';
import { CardTemplateDto } from './dto/card-template.dto';
import { ServiceType } from './models/service-type.enum';
import { CardTemplate, CardTemplateDocument, CardTemplateId } from './schemas/card-template.schema';
import { CheckupTemplate, CheckupTemplateDocument } from './schemas/checkup-template.schema';
import { Diagnosis } from './schemas/diagnosis.nested.schema';
import { LfkTemplate, LfkTemplateDocument } from './schemas/lfk-template.schema';

@Injectable()
export class CardTemplateService {
    constructor(
        @InjectModel(CardTemplate.name)
        private cardTemplateModel: Model<CardTemplateDocument>,

        @InjectModel(CheckupTemplate.name)
        private checkupTemplateModel: Model<CheckupTemplateDocument>,

        @InjectModel(LfkTemplate.name)
        private lfkTemplateModel: Model<LfkTemplateDocument>,

        private readonly handbooksService: HandbooksService,
    ){}

    async create(cardTemplateDto: CardTemplateDto, userId: UserId) {
        switch (cardTemplateDto.serviceType) {
            case ServiceType.Checkup:
                return this.createCheckupTemplate(
                    { 
                        ...cardTemplateDto, 
                        user: userId, 
                        diagnosis: { diagnosisId: cardTemplateDto.diagnosisId },
                        outcome: { outcomeId: cardTemplateDto.outcomeId },
                        result: { resultId: cardTemplateDto.resultId }
                    },
                    cardTemplateDto.medDepId
                );
            case ServiceType.LFK:
                return this.createLfkTemplate({ ...cardTemplateDto, user: userId });
            default:
                throw new BadRequestException(`serviceType = ${cardTemplateDto.serviceType} is uncorrect.`);
        }
    }

    async get(serviceType: ServiceType, userId: UserId) {

        const templates = this.cardTemplateModel.find({ serviceType: serviceType, user: userId });

        return templates;
    }

    async getOne(id: CardTemplateId) {

        const template = await this.cardTemplateModel.findById(id);

        if(!template) {
            throw new NotFoundException('Template not found');
        }

        return template;
    }

    async delete(id: CardTemplateId) {

        const deletedTemplate = await this.cardTemplateModel.findByIdAndDelete(id);

        if(!deletedTemplate) {
            throw new NotFoundException('Template not found');
        }

        return deletedTemplate;
    }

    private async createCheckupTemplate(checkupTemplateData: CheckupTemplate, medDepId: string):Promise<CheckupTemplateDocument> {


        //TODO добавить получение диагноза по ID
        checkupTemplateData.diagnosis = await this.handbooksService.getDiagnosisById(
            medDepId, 
            checkupTemplateData.diagnosis.diagnosisId
        );

        checkupTemplateData.result = await this.handbooksService.getResultById(
            medDepId, 
            checkupTemplateData.result.resultId
        );
        checkupTemplateData.outcome = await this.handbooksService.getOutcomeById(
            medDepId, 
            checkupTemplateData.outcome.outcomeId
        );

        const createdCheckupTemplate = new this.checkupTemplateModel(checkupTemplateData);
        return createdCheckupTemplate.save();
    }

    private async createLfkTemplate(lfkTemplateData: LfkTemplate):Promise<LfkTemplateDocument> {

        const createdLfkTemplate = new this.lfkTemplateModel(lfkTemplateData);
        return createdLfkTemplate.save();
    }
}
