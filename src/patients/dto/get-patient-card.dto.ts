export interface GetPatientCardDto {
    medDepId: string;
    page: number;
    searchString: string;
    serviceType: Number;
}