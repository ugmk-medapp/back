export class GetPatientsDto {
    medDepId: string;
    page: number;
    searchName: string;
}