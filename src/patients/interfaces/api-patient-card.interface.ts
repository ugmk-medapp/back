export interface ApiPatientCard {
    patients_id: number;
    timestamp: string;
    programma_lfk: string;
    version: any;   // не знаю чо там за тип
    id: number;
    medecins_nom: string;
    progressiya: string;
    date_consultation: string;
    ad_posle_lfk: any;
    zhaloby: string;
    ad_do_lfk: any;
    outcomes_id: number; // исход
    outcomes_name: string;
    result_name: string;
    result_id: number; 
    motif_de_consultation: string;
    naznaheniq_i_rekomendacii: string;
    obhij_osmotr: string; // оъективно
    ds_name: string;
    type: 0 | 1| 2;
    ev_close: boolean;
    istoriq_zabolevaniq: string;
    expertniy_anamnez: string;
    ds_description: string | null;
    ds_id: number;
}