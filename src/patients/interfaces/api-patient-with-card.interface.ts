import { ApiPatientCard } from "./api-patient-card.interface";

export interface ApiPatientWithCard {
    prenom: string;
    nom: string;
    patronyme: string;
    ne_le: string;
    source: Array<ApiPatientCard>;
}