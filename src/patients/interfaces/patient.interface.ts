export interface Patient {
    name: string;
    surname: string;
    patronymic: string;
    birthYear: string;
    patientId: number;
}