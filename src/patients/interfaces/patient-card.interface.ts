export interface PatientCard {
    program: string; // программа ЛФК
    id: number;  //отправлю, вдруг понадобится
    doctorFullName: string;
    progression: string; // прогрессия
    dateConsultation: string;
    complaints: string; //жалобы
    APbefore: string; // AP - arterial pressure (Артериальное давление)
    APafter: string;  // AP - arterial pressure (Артериальное давление)
    diagnosis: string; // код диагноза (возможно)
    outcome: string;
    result: string;
    diseaseHistory: string;
    objectively: string;
    recommendations: string;
    isClose: boolean;
    serviceType: 0 | 1 | 2;
    expertAnamnesis: string;
    diagnosisDescription: string | null; // текстовое описание диагноза
    diagnosisId: number;
    resultId: number;
    outcomeId: number;
}