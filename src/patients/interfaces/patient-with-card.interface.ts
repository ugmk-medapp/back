import { PatientCard } from "./patient-card.interface";

export interface PatientWithCard {
    name: string;
    surname: string;
    patronymic: string;
    birthYear: string;
    diagnosticResults: Array<PatientCard>;
}