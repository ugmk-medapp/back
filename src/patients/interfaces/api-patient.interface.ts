
export interface ApiPatient {
    prenom: string;
    nom: string;
    patronyme: string;
    timestamp: string;
    version: number | null;
    patients_id: number;
    ne_le: string | null;
}
