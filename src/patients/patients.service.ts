import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { FetchService } from 'src/fetch/fetch.service';
import { OrganizationsService } from 'src/organizations/organizations.service';
import { GetPatientCardDto } from './dto/get-patient-card.dto';
import { ApiPatientCard } from './interfaces/api-patient-card.interface';
import { ApiPatientWithCard } from './interfaces/api-patient-with-card.interface';
import { ApiPatient } from './interfaces/api-patient.interface';
import { PatientCard } from './interfaces/patient-card.interface';
import { PatientWithCard } from './interfaces/patient-with-card.interface';
import { Patient } from './interfaces/patient.interface';

@Injectable()
export class PatientsService {
    constructor(
        private readonly organizationsService: OrganizationsService,
        private readonly fetchService: FetchService
    ){}

    /**
     * 
     * @param organizationId id Организации
     * @param page номер страницы из 10 элементов
     * @param seachName строка поиска по имени
     * @returns возвращает массив с пациентами
     */
    public async getPatientsWithCards(
        organizationId: string, 
        page: number, 
        searchName: string): Promise<Patient[]> {

        const { address, token } = await this.organizationsService.getOrganizationApi(organizationId);

        const response = await this.fetchService.post(
            `${address}GetPatietns/${page}`,
            { query: searchName },
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            }
        )

        if(!response.ok) {
            console.log(response);
            throw new InternalServerErrorException('external api get patients error');
        }

        const apiPatients: ApiPatient[] = (await response.json()).data;
  
        return apiPatients.map((patient: ApiPatient) => this.toPatient(patient) );
    }


    public async getCard(patientId: number, getPatientCardDto: GetPatientCardDto): Promise<PatientWithCard> {

        const { medDepId, page, searchString, serviceType } = getPatientCardDto;
        const { 
            address, 
            token 
        } = await this.organizationsService.getOrganizationApi(medDepId);

        const response = await this.fetchService.post(
            `${address}GetMotconsu/${patientId}/${page}`,
            { 
                query: searchString,
                type: +serviceType
            },
            {
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                }
            }
        )

        if(!response.ok) {
            if(response.status === 404) {
                throw new NotFoundException('patient card not exist');
            }
            throw new InternalServerErrorException('external api get card error');
        }

        const apiPatientWithCard: ApiPatientWithCard = (await response.json()).data;

        return this.toPatientWithCard(apiPatientWithCard);
    }


    private toPatient(apiPatient: ApiPatient): Patient {
        const birthTime = apiPatient.ne_le ? apiPatient.ne_le.split('T') : null; // формат "0001-01-01T00:00:00"
        const birthYear = birthTime && birthTime[0] ? (birthTime[0].split('-'))[0] || 'не указан' : 'не указан';

        const patient: Patient = {
            name: apiPatient.prenom,
            surname: apiPatient.nom,
            patronymic: apiPatient.patronyme,
            birthYear,
            patientId: apiPatient.patients_id
        }
        return patient;
    }

    private toPatientWithCard(apiPatientWithCard: ApiPatientWithCard): PatientWithCard {
        console.log(apiPatientWithCard);
        const { nom, prenom, patronyme, ne_le } = apiPatientWithCard;

        const birthTime = ne_le ? ne_le.split('T') : null; // формат "0001-01-01T00:00:00"
        const birthYear = birthTime && birthTime[0] ? (birthTime[0].split('-'))[0] || 'не указан' : 'не указан';

        console.log(apiPatientWithCard)

        const diagnosticResults: Array<PatientCard> = apiPatientWithCard.source.map(
            dResult => ({
                program:              dResult.programma_lfk,
                id:                   dResult.id,  
                doctorFullName:       dResult.medecins_nom,
                progression:          dResult.progressiya,
                dateConsultation:     dResult.date_consultation,
                complaints:           dResult.zhaloby || dResult.motif_de_consultation,
                APbefore:             dResult.ad_do_lfk,
                APafter:              dResult.ad_posle_lfk,
                diagnosis:            dResult.ds_name,
                outcome:              dResult.outcomes_name,
                result:               dResult.result_name,
                diseaseHistory:       dResult.istoriq_zabolevaniq,
                objectively:          dResult.obhij_osmotr,
                recommendations:      dResult.naznaheniq_i_rekomendacii,
                isClose:              dResult.ev_close,
                serviceType:          dResult.type,
                expertAnamnesis:      dResult.expertniy_anamnez,
                diagnosisDescription: dResult.ds_description,
                diagnosisId:          dResult.ds_id,
                resultId:             dResult.outcomes_id,
                outcomeId:            dResult.result_id
            })
        )

        const patient: PatientWithCard = {
            name: prenom,
            surname: nom,
            patronymic: patronyme,
            birthYear,
            diagnosticResults
        }      

        return patient;
    }
}
