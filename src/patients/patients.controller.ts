import { Query, Controller, Get, Param, UseGuards } from '@nestjs/common';
import { GetPatientsDto } from './dto/get-patients.dto';
import { GetPatientCardDto } from './dto/get-patient-card.dto';
import { Patient } from './interfaces/patient.interface';
import { PatientsService } from './patients.service';
import { PatientWithCard } from './interfaces/patient-with-card.interface';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('patients')
export class PatientsController {
    constructor(
        private readonly patientsService: PatientsService
    ){}

    @UseGuards(JwtAuthGuard)
    @Get()
    async patientsGet(@Query() query: GetPatientsDto): Promise<Patient[]> {
        return this.patientsService.getPatientsWithCards(query.medDepId, query.page, query.searchName);
    }

    @UseGuards(JwtAuthGuard)
    @Get(':patientId/card')
    async patientCardGet(
        @Query() query: GetPatientCardDto,
        @Param('patientId') patientId: number
    ): Promise<PatientWithCard> {
        return this.patientsService.getCard(patientId, query);
    }
}
