import { Module } from '@nestjs/common';
import { FetchModule } from 'src/fetch/fetch.module';
import { OrganizationsModule } from 'src/organizations/organizations.module';
import { PatientsService } from './patients.service';
import { Agent } from 'https';
import { PatientsController } from './patients.controller';

@Module({
  imports: [
    FetchModule.register({
      agent: new Agent({rejectUnauthorized: false }),
    }),
    OrganizationsModule,
  ],
  providers: [PatientsService],
  controllers: [PatientsController]
})
export class PatientsModule {}
