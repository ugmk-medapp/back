import { Module, DynamicModule } from '@nestjs/common';
import { FetchService } from './fetch.service';
import { FetchOptions } from './interfaces/fetch-options.interface';

@Module({
})
export class FetchModule {
  static register(fetchOptions?: FetchOptions): DynamicModule {
    return {
      module: FetchModule,
      providers: [
        {
          provide: 'FETCH_OPTIONS',
          useValue: fetchOptions || {}
        },
        FetchService
      ],
      exports: [FetchService]
    }
  }
}
