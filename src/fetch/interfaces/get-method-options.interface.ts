import { HeadersInit } from 'node-fetch';


export interface GetMethodOptions {
    body?: any;
    headers?: HeadersInit;
    follow?: number,
    timeout?: number,
    compress?: boolean,
    size?: number,
    agent?: any
}