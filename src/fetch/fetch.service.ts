import { Inject, Injectable } from '@nestjs/common';
import fetch, { RequestInit, Response } from 'node-fetch';
import { FetchOptions } from './interfaces/fetch-options.interface';
import { GetMethodOptions } from './interfaces/get-method-options.interface';

import { Agent as httpsAgent } from 'https';

import { Agent as httpAgent } from 'http';

@Injectable()
export class FetchService {
    constructor(
        @Inject('FETCH_OPTIONS') private globalOptions: FetchOptions
    ) {}

    async get(url: string, params?: Object, options?: GetMethodOptions): Promise<Response> {

        if(!url) console.log('ERROR get fetch. url not exist');

        const agent = this.getAgent(url);

        const fullOptions: RequestInit = {
            method: 'GET',
            ...this.globalOptions,
            ...options,
            agent: agent
        }
        let urlParams = '';
        if(params) {
            urlParams = this.generateUrlParams(params);
        }
        const encodeUrl = `${url}${urlParams}`;
        const response = fetch(encodeUrl, fullOptions);

        return response;
    }

    async post(url: string, body?: Object, options?: GetMethodOptions): Promise<Response> {

        if(!url) console.log('ERROR get fetch. url not exist');

        const agent = this.getAgent(url);

        const fullOptions: RequestInit = {
            method: 'POST',
            body: JSON.stringify(body),
            ...this.globalOptions,
            ...options,
            agent: agent
        }
        const response = fetch(url, fullOptions);

        return response;
    }

    private generateUrlParams(params: Object): string {
        let url = '?';
        for(let paramName in params) {
            url += `${paramName}=${params[paramName]}&`;
        }

        return url;
    }

    private getAgent(url: string): any {
        const protocol:string = url.split(':')[0];
        console.log('PROTOCOL', protocol);
        if(protocol === 'https') {
            return new httpsAgent ({
                rejectUnauthorized: false,
            });
        } else if (protocol === 'http'){
            console.log('http protocol же')
            return new httpAgent();
        }
    }
}
