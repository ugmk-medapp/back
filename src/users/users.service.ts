import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getManager, EntityManager, Like } from 'typeorm';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from './dto/create-user.dto';
import { User, UserDocument } from './schemas/users.schema';
import { Medecins } from 'src/entities/Medecins';
import { ConfirmCode, ConfirmCodeDocument } from './schemas/confirm-code.schema';
import { EmailService } from 'src/email/email.service';
import { ConfirmUserDto } from './dto/confirm-user.dto';
import { FullUser } from './interfases/full-user.interface';
import { StaffUnit } from './interfases/staff-unit.interface';

@Injectable()
export class UsersService {
    private saltOrRounds: number;
    private entityManager: EntityManager;
    constructor(
        @InjectModel(User.name)
        private userModel: Model<UserDocument>,

        @InjectModel(ConfirmCode.name)
        private confirmCodeModel: Model<ConfirmCodeDocument>,
        
         @InjectRepository(Medecins)
         private medecinsRepository: Repository<Medecins>,
        
        private emailService: EmailService
    ) {
        this.saltOrRounds = 10;
        this.entityManager = getManager();
    }

    private generateCode(): string {
        let code: string = '';

        for (let i: number = 0; i < 4; i++) {
            let num: number = Math.floor(Math.random() * (9 - 1 + 1) + 1);
            code += num.toString();
        }

        return code;
    }

    public async getWaitingTimeCode(email: string): Promise<number> {

        const lowerEmail = email.toLowerCase();

        const oldCode = await this.confirmCodeModel.findOne({ email: lowerEmail });
        if(oldCode) {
            if ( (Date.now() - oldCode.msec_create) < 60000) {
                // если со времени создания кода прошло меньше минуты, то возвращаем сколько осталось ждать в сек.
                return Math.round( ( 60000 - ( Date.now() - oldCode.msec_create) ) / 1000);
            }
        }

        // если кода нет или время прошло, то можно отправлять заново
        return 0;
    }

    public async sendCode( email: string, action: string ): Promise<void> {

        const lowerEmail = email.toLowerCase();

        const user = await this.userModel.findOne({ email: lowerEmail });

        if(!user) {
            throw new NotFoundException('user not found');
        }

        const timeCode = await this.getWaitingTimeCode(lowerEmail);
        
        if(timeCode !== 0) {
            throw new ForbiddenException('wait');
        }

        await this.confirmCodeModel.findOneAndDelete({ email: lowerEmail });

        const code: string = this.generateCode();
        const createdConfirmCode = new this.confirmCodeModel({ code, email: lowerEmail, msec_create: Date.now() });
        await createdConfirmCode.save();

        if(action === 'C') {
            await this.emailService.sendEmailAfterCreateUser(email, code);
        }
        else await this.emailService.sendEmailRestorePassword(email, code);
    }

    async create(createUserDto: CreateUserDto): Promise<User> {

        const lowerEmail = createUserDto.email.toLowerCase();

        const oldUser = await this.userModel.findOne({ email: lowerEmail });

        if(oldUser) {
            throw new ForbiddenException('user exist');
        }

         const medicin = await this.medecinsRepository.findOne({ email: Like(lowerEmail) });

        if(!medicin) {
            throw new NotFoundException('email not found');
        }
        const hashPassword: string = await bcrypt.hash(createUserDto.password, this.saltOrRounds);

        // замена пароля на хэш
        const userObj = {
            email: lowerEmail, 
            password: hashPassword,
            external_id: medicin.id,
        };
        const createdUser = new this.userModel(userObj);
        const user = await createdUser.save();

        await this.sendCode(createUserDto.email, 'C');

        return user;
    }

    async createWithPhone(phone: string): Promise<UserDocument> {
        
        const medicin = await this.medecinsRepository.findOne({ telefon: phone });
        if(!medicin) {
            throw new NotFoundException('Пользователь с таким телефоном не найден.');
        }

        const { email, id } = medicin;
        const lowerEmail = email ? email.toLowerCase() : '';
        const oldUser = await this.userModel.findOne({ email: lowerEmail });

        if(!oldUser) {
            const newUser = new this.userModel({
                email: lowerEmail,
                phone,
                external_id: id,
                is_verified: true
            })

            return newUser.save();
        }

        if(oldUser && !oldUser.phone) {
            return this.userModel.findByIdAndUpdate(oldUser._id, { phone }, { new: true }).exec();
        }

        return oldUser;
    }

    async restorePassword(user_id: string, password: string): Promise<User> {
        let user = await this.userModel.findById(user_id).select('+confirm_update_time');

        // TODO: вынести эти все проверки на существование юзера в отдельную функцию findById
        if(!user) {
            throw new NotFoundException('user not found');
        }

        // милисикунды. если confirm_update_time = 0, то условие ниже будет ложно. (нужно для сброса подтверждения)
        const confirmTime: number = Date.now() - user.confirm_update_time;

        // если больше 10 минут прошло с момента подтверждения
        if(confirmTime > 600000) {
            throw new ForbiddenException('forbidden update password. time is up.')
        }

        const hashPassword: string = await bcrypt.hash(password, this.saltOrRounds);

        user = await this.userModel.findByIdAndUpdate(
            user_id, 
            { 
                password: hashPassword, 
                confirm_update_time: 0 
            }, 
            { new: true },
        );
        return user;
    }

    async confirmUserEmail({ email, code, action }: ConfirmUserDto): Promise<User> {
        const lowerEmail = email.toLowerCase();

        let user = await this.userModel.findOne({ email: lowerEmail });
        if(!user) {
            throw new NotFoundException('user not found');
        }
        const confirmCode = await this.confirmCodeModel.findOne({ email: user.email });

        if(!confirmCode) {
            throw new NotFoundException('code not found');
        }
        if(confirmCode.code !== code) {
            throw new ForbiddenException('code is incorrect');
        }

        await this.confirmCodeModel.findByIdAndDelete(confirmCode._id);

        if(action === 'C') {
            user = await this.userModel.findByIdAndUpdate(user._id, { is_verified: true }, { new: true });
            return user;
        }
        user = await this.userModel.findByIdAndUpdate(user._id, { is_verified: true, confirm_update_time: Date.now() }, { new: true });

        return user;
    }

    async findOneWithPassword(email: string): Promise<User | undefined> {
        return this.userModel.findOne({ email: email.toLowerCase() }).select('+password');
    }

    async findById(id: string) {
        return this.userModel.findById(id);
    }

    // получение пользователя вместе с ФИО и штатными единицами.
    async getFullUser(id:string):Promise<FullUser>{
        const user = await this.userModel.findById(id);

        if(!user) throw new NotFoundException('user not found');

        const medicin = await this.medecinsRepository.findOne(user.external_id);

        if(!medicin) throw new NotFoundException('user not valid');

        const staffUnits: StaffUnit[] = await this.entityManager.query(
            `SELECT 
                Meddep.Id as medDepId, 
                Meddep.MEDDEP_ID as externalMedDepId,
                MedicalOrganizations.OrganizationName + 
                    CASE WHEN FmDep.LABEL IS NOT NULL 
                        THEN ' / ' + FmDep.LABEL 
                        ELSE '' 
                    END + 
                    CASE WHEN Prof.NOM IS NOT NULL 
                        THEN ' / ' + Prof.NOM 
                        ELSE '' 
                    END as name
            FROM Medecins
            JOIN OrganizationMedecins ON OrganizationMedecins.MedecinsId = Medecins.Id
            JOIN OrganizationMeddep ON OrganizationMedecins.OrganizationId = OrganizationMeddep.OrganizationId
            JOIN Meddep ON Meddep.MEDECINS_ID = Medecins.MEDECINS_ID AND OrganizationMeddep.MeddepId = Meddep.Id
            JOIN MedicalOrganizations ON MedicalOrganizations.Id = OrganizationMeddep.OrganizationId
            OUTER APPLY (
                SELECT FmDep.LABEL FROM FmDep 
                    JOIN OrganizationFmdep ON FmDep.Id = OrganizationFmdep.FmDepId AND OrganizationFmdep.OrganizationId = OrganizationMedecins.OrganizationId
                WHERE Meddep.FM_DEP_ID = FmDep.FM_DEP_ID
            ) FmDep
            OUTER APPLY (
                SELECT Prof.NOM FROM Medecins Prof 
                    JOIN OrganizationMedecins OM ON Prof.Id =  OM.MedecinsId 
                    AND OM.OrganizationId = OrganizationMedecins.OrganizationId
                WHERE Prof.MEDECINS_ID = Meddep.MEDECINS_PROFILE_ID
            ) Prof  
           WHERE ISNULL(Meddep.ARCHIVE,0) = 0 
           AND Medecins.EMAIL = @0`,
           [ medicin.email ]
        );
        
       return {
           _id: user._id,
           email: user.email,
           external_id: user.external_id,
           is_verified: user.is_verified,
           surnameAndInitials: this.getSurnameAndInitals(medicin.nom, medicin.prenom),
           staffUnits: staffUnits
       }
    }


    private getSurnameAndInitals(surname: string, nameAndPatronymic:string):string {
        if(!nameAndPatronymic){
            return surname;
        }
        const splitedNameAndPatronymic = nameAndPatronymic.split(' ');

        if(splitedNameAndPatronymic.length > 1) {

            const name = splitedNameAndPatronymic[0];
            const patronumic = splitedNameAndPatronymic[1];

            const nameInital = name ? name[0].toUpperCase() : '';
            const patronymicInital = patronumic ? patronumic[0].toUpperCase() : '';
            return `${surname} ${nameInital}${nameInital ? '.' : ''}${patronymicInital}${patronymicInital ? '.' : ''}`;
        }

        if(splitedNameAndPatronymic.length === 1) {

            const name = splitedNameAndPatronymic[0];

            const nameInital = name ? name[0].toUpperCase() : '';

            return `${surname} ${nameInital ? `${nameInital}.` : ''}`;
        }

    }
}
