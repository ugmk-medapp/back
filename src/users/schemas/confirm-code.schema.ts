import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ConfirmCodeDocument = ConfirmCode & Document;

@Schema()
export class ConfirmCode {
    @Prop({ required: true })
    code: string;

    @Prop({ 
        required: true, 
        default: Date.now,
        expires: 600,
        select: false
    })
    create_time: Date;

    @Prop({ 
        required: true,
        unique: true,
    })
    email: string;

    @Prop({
        required: true
    })
    msec_create: number; 
}

export const ConfirmCodeSchema = SchemaFactory.createForClass(ConfirmCode);