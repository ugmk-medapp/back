import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { MongoId } from 'src/models/mongo-id.interface';

export type UserDocument = User & Document;

export type UserId = MongoId<UserDocument>;

@Schema()
export class User {
    @Prop({
        required: true,
        unique: true,
    })
    email: string;

    @Prop({
        unique: true,
    })
    phone: string;

    //Хэш пароля
    @Prop({
        select: false
     })
    password: string;

    @Prop({ default: false })
    is_verified: boolean;

    // здесь записывается результат функции Date.now()
    // она возвращает кол-во мсек с 1970г.
    @Prop({ 
        default: 0,
        select: false
    })
    confirm_update_time: number;

    @Prop({ 
        required: true,
        unique: true
    })
    external_id: string;

    @Prop({
        default: false
    })
    is_telegram: boolean;
}

export const UserSchema = SchemaFactory.createForClass(User);