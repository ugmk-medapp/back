import { StaffUnit } from "./staff-unit.interface";

export interface FullUser {
    _id: string,
    email: string,
    external_id: string,
    is_verified: boolean,
    surnameAndInitials: string,
    staffUnits: StaffUnit[],
}