export interface StaffUnit {
    medDepId: string,
    externalMedDepId: number,
    name: string
}