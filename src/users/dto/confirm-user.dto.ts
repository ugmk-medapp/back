export class ConfirmUserDto {
    code: string;
    email: string;
    action: 'C' | 'U'; // U (update) or C (create)
}