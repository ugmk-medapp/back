export class SendCodeDto {
    email: string;
    action: 'C' | 'U'; // C - create or U - update
}