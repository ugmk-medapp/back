import { Controller, Post, Get, Body, Query, Req, UseGuards, Put, Param } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { ConfirmUserDto } from './dto/confirm-user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { SendCodeDto } from './dto/send-code.dto';
import { FullUser } from './interfases/full-user.interface';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
    constructor(private usersServise: UsersService){}

    @Post()
    async createUser(@Body() body: CreateUserDto) {
        const user = await this.usersServise.create(body);
        return user;
    }

    @Post('confirm')
    async confirmUser(@Body() body: ConfirmUserDto) {
        const user = await this.usersServise.confirmUserEmail(body);
        return user;
    }

    @Post('sendCode')
    async restoreStart (@Body() body: SendCodeDto) {
        return this.usersServise.sendCode(body.email, body.action);
    }

    @Put('restore/:user_id')
    async restorePassword(
        @Param('user_id') user_id: string, 
        @Body('password') password: string 
    ) {
        return this.usersServise.restorePassword(user_id, password);
    }

    @Get('codeWaitingTime')
    async getWaitingTimeCode(@Query('email') email: string) {
        const time: number = await this.usersServise.getWaitingTimeCode(email);
        return {
            time: time,
        }
    }

    @UseGuards(JwtAuthGuard)
    @Get('/me')
    async getUser(@Req() req): Promise<FullUser> {
        const fullUser = await this.usersServise.getFullUser(req.user._id);
        return fullUser;
    }
}
